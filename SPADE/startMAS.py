import spade
from managerAgent import ManagerAgent
from clientAgent import ClientAgent
from instructorAgent import InstructorAgent


async def main():

    manager = ManagerAgent("masmanager@hot-chilli.eu", "Manager001")
    instructor = InstructorAgent("masinstructor@hot-chilli.eu", "Instructor001")
    client = ClientAgent("client@hot-chilli.eu", "Client001")

    await manager.start()
    await instructor.start()
    await client.start()


if __name__ == "__main__":
    spade.run(main())
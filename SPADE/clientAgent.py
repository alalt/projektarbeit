import spade
from spade.agent import Agent
from spade.behaviour import FSMBehaviour, State
from spade.message import Message
from spade.template import Template
from dataclasses import dataclass
import random
import asyncio
import time

STATE_ONE = "STATE_ONE"
STATE_TWO = "STATE_TWO"
STATE_THREE = "STATE_THREE"
STATE_FOUR = "STATE_FOUR"
managers = ["masmanager@hot-chilli.eu"]


#
#   Creating the Client Agent
#
class ClientAgent(Agent):
    async def setup(self):
        self.SpotManager = ""
        self.spot= ""

        fsm = RequestBehav()
        fsm.add_state(name=STATE_ONE, state=StateOne(), initial=True)
        fsm.add_state(name=STATE_TWO, state=StateTwo(self))
        fsm.add_state(name=STATE_THREE, state=StateThree())
        fsm.add_state(name=STATE_FOUR, state=StateFour(self))
        fsm.add_transition(source=STATE_ONE, dest=STATE_TWO)
        fsm.add_transition(source=STATE_TWO, dest=STATE_THREE)
        fsm.add_transition(source=STATE_THREE, dest=STATE_FOUR)
        fsm.add_transition(source=STATE_TWO, dest=STATE_ONE)
        fsm.add_transition(source=STATE_THREE, dest=STATE_ONE)
        self.add_behaviour(fsm)

#
#   Request Behaviour
#  
class RequestBehav(FSMBehaviour):
    
    async def on_start(self):
        print("Starting Client")

    async def on_end(self):
        print(f"FSM finished at state {self.current_state}")
        await self.agent.stop()

#
#   Request State 1
#   Sending Requests to all known Mangers
#
class StateOne(State):
    async def run(self):
        for x in GetManagers():
            msg = Message(to=x)                             # Instantiate the message
            msg.set_metadata("performative", "request")     # Set the "request" FIPA performative
            msg.body = "Parking request"                    # Set the message content
            await self.send(msg)
            
        print("All requests sent!")

        self.set_next_state(STATE_TWO)

                
#
#   Accept Proposal state 2
#   Waiting for all answers from managers and accept one
#
class StateTwo(State):

    def __init__(self,agent):
        super().__init__()
        self.agent = agent

    async def run(self):
        print("")
        tempProposal = Template()
        tempProposal.set_metadata("performative", "propose")
        countAnswers = 0
        self.agent.SpotManager = ""

        while countAnswers < len(GetManagers()):
            msg = await self.receive(timeout = 10)
            if msg and msg.get_metadata("performative") == tempProposal.get_metadata("performative"):
                print("Proposal received")
                countAnswers += 1
                #
                #   select best proposed parking spot
                #
                self.agent.SpotManager = format(msg.sender)
                self.agent.spot = msg.body
            else:
                countAnswers += 1
                print("Fail")
        
        if self.agent.SpotManager != "":
            msg = Message(to=self.agent.SpotManager)
            msg.set_metadata("performative", "accept_proposal")     # Set the "accept_proposal" FIPA performative
            msg.body = self.agent.spot
            await self.send(msg)
            print("Sending Accept_Proposal")
            self.set_next_state(STATE_THREE)
        else:
            self.set_next_state(STATE_ONE)
        


#
#   Get Instructor information state 3
#   Waiting for informations of the right instructor to follow
#
class StateThree(State):
    async def run(self):
        tempProposal = Template()
        tempProposal.set_metadata("performative", "inform")
        tempCancel = Template()
        tempCancel.set_metadata("performative", "cancel")

        msg = await self.receive(20)

        if msg and msg.get_metadata("performative") == tempProposal.get_metadata("performative"):
            print("Following Instructor and Parking")
            #######################
            # Section: Parking in Simulation
            #######################
            self.set_next_state(STATE_FOUR)
        elif msg and msg.get_metadata("performative") == tempCancel.get_metadata("performative"):
            print("Parking process got canceled! Restarting the Process.")
            self.set_next_state(STATE_ONE)

#
#   Leaving state 4
#   Inform manager if leaving the parking spot
#
class StateFour(State):

    def __init__(self,agent):
        super().__init__()
        self.agent = agent

    async def run(self):
        print("Waiting!")
        time.sleep(random.randint(5,15))
        if self.agent.SpotManager != "":
            msg = Message(to=self.agent.SpotManager)
            msg.set_metadata("performative", "inform")
            msg.body = "Leaving"
            await self.send(msg)
            print("Leaving!")
        else:
            print("Fail at STATE_FOUR (leaving state)")
        # no final state is setted, since this is a final state


#   Function to get the known managers    
def GetManagers():
    return managers


#   Main class
#async def main():
#    client = ClientAgent("client@hot-chilli.eu", "Client001")
#    await client.start()
#
#    await spade.wait_until_finished(client)
#    await client.stop()
#    print("Agent finished")

#if __name__ == "__main__":
#    spade.run(main())
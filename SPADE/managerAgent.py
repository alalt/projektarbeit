import spade
from spade.agent import Agent
from spade.behaviour import OneShotBehaviour,CyclicBehaviour
from spade.message import Message
from spade.template import Template
from dataclasses import dataclass
import aioxmpp
from aioxmpp import PresenceShow
import sys

# masmanager@hot-chilli.eu    Manager001

@dataclass
class ParkingSpot:
    free: bool
    client: str

class ManagerAgent(Agent):
    spots = []
    instructors = ["masinstructor@hot-chilli.eu"]

    #
    #   Setup
    #
    async def setup(self):
        self.createSpots(self, int (5))
        print("Manager Agent started")
        a = self.RequestBehav(self)
        b = self.AcceptBehav(self)
        c = self.LeavingBehav(self)

        self.add_behaviour(a)
        self.add_behaviour(b)
        self.add_behaviour(c)
    
    #
    # Request Behaviour
    #
    class RequestBehav(CyclicBehaviour):
        def __init__(self, agent):
            super().__init__()
            self.agent = agent

        async def run(self):
            template = Template()
            template.set_metadata("performative", "request")

            msg = await self.receive(timeout=30) # wait for a message for 30 seconds

            if msg and msg.get_metadata("performative") == template.get_metadata("performative"):
                print("Request received")
                spot = self.agent.checkSpots(self.agent)
                if spot != None:
                    rep = Message(to=format(msg.sender))
                    rep.set_metadata("performative", "propose")             # Set the "propose" FIPA performative
                    rep.body = format(spot)                                 # Set the message content

                    await self.send(rep)
                    print("Send proposal")
                else:
                    print("No spot free")
                    rep = Message(to=format(msg.sender))
                    rep.set_metadata("performative", "cancel")
                    rep.body = "No spot free"                

                    await self.send(rep)

    #
    # Accept Behaviour
    #
    class AcceptBehav(CyclicBehaviour):
        def __init__(self, agent):
            super().__init__()
            self.agent = agent

        async def run(self):
            template = Template()
            template.set_metadata("performative", "accept_proposal")
            msg = await self.receive(timeout=10)
            if msg and msg.get_metadata("performative") == template.get_metadata("performative"):
                print("Accept_proposal received")
                next = self.agent.InstructorBehav(self, format(msg.sender), int(msg.body))
                self.agent.add_behaviour(next)
    
    #
    # Instructor Behaviour
    #
    class InstructorBehav(OneShotBehaviour):
        def __init__(self, agent, sender, spot):
            super().__init__()
            self.agent = agent
            self.sender = sender
            self.spot = spot

        async def run(self):

            #   Ask all known Instructors if they are ready
            #
            readyInstr = []
            tempAccept = Template()
            tempAccept.set_metadata("performative", "agree")
            tempCancel = Template()
            tempCancel.set_metadata("performative", "cancel")

            for x in self.agent.instructors:
                msg = Message(to=x)                                 # Instantiate the message
                msg.set_metadata("performative", "propagate")       # Set the "propagate" FIPA performative
                await self.send(msg)
            
            print("Send Propagate to intructors")

            #   Wait for all answers
            #
            countAttempts = 0
            countAnswers = 0

            while countAttempts <= 6 and countAnswers < len(self.agent.instructors):
                msg = await self.receive(timeout=10)

                if msg and msg.get_metadata("performative") == tempAccept.get_metadata("performative"):
                    readyInstr.append(msg.sender)
                    countAnswers += 1
                    print("Confirm recieved")

                elif msg and msg.get_metadata("performative") == tempCancel.get_metadata("performative"):
                    countAnswers += 1
                    print("Cancel recieved")

                #else:
               #    print(".", end="")
                
                countAttempts += 1
            
            #   Give the selected instructor the Task to instruct the Cardriver to the parking spot    
            #      
            if len(readyInstr) > 0:
                msg = Message(to=format(readyInstr[0]))
                msg.set_metadata("performative", "request")
                msg.body = "Task: Instruct Car"
                await self.send(msg)
                print("Sending Request to instructor")
            else:
                print("No Instructor ready")
            
            #   if the instructor confirms the task inform the client
            #
            temp = Template()
            temp.set_metadata("performative", "confirm")
            ans = await self.receive(timeout=10)
            if ans and ans.get_metadata("performative") == temp.get_metadata("performative"):
                print("Accept_proposal received")
                msg = Message(to=self.sender)
                msg.set_metadata("performative", "inform")
                msg.body = "Follow Instructor" + format(ans.body)
                await self.send(msg)
                print("Sending Inform to Client")

                # safe client to spot
                self.agent.spots[self.spot].free = False
                self.agent.spots[self.spot].client = self.sender
                self.agent.printSpots(self.agent)
            self.kill(exit_code=10)
    

    #
    #   Leaving Behaviour
    #
    class LeavingBehav(CyclicBehaviour):
        def __init__(self, agent):
            super().__init__()
            self.agent = agent

        async def run(self):
            temp = Template()
            temp.set_metadata("performative", "inform")
            msg = await self.receive(timeout=5)
            if msg and msg.get_metadata("performative") == temp.get_metadata("performative"):
                print("Got a Leaving Message")
                self.agent.clearSpot(self.agent, format(msg.sender))
                self.agent.printSpots(self.agent)
    
    
    #
    #   Method to create a group of parking spots
    #
    def createSpots(self, agent, number):
        counter = 0
        while counter < number:
            new_spot  = ParkingSpot(True ,"")
            agent.spots.append(new_spot)
            counter += 1
        print("Created " + format(counter) + " parking spots")
    
    #
    #   returns the number of a free spot
    #
    def checkSpots(self, agent):
        counter = 0
        while counter < len(agent.spots):
            if agent.spots[counter].free:
                return counter
        return None
    
    #
    #   clears the spot if Client is leaving
    #
    def clearSpot(self, agent, client):
        counter = 0
        while counter < len(agent.spots):
            if agent.spots[counter].free == False and agent.spots[counter].client == client:
                agent.spots[counter].free = True
                agent.spots[counter].client = ""
            counter += 1
    
    #
    #   prints the list of spots with clients in console
    #
    def printSpots(self, agent):
        print("Spots:")
        for x in self.spots:
            print("- " + x.client)

#async def main():
#    if len(sys.argv) == 2:
#        Manager = ManagerAgent("masmanager@hot-chilli.eu", "Manager001")
#        await Manager.start(auto_register=True)
#
#        await spade.wait_until_finished(Manager)
#        print("Agents finished")
#    else:
#        print("A number of parking spots is needed to start Manager Agent")

#if __name__ == "__main__":
#    spade.run(main())

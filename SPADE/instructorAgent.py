import spade
from spade.agent import Agent
from spade.behaviour import OneShotBehaviour, CyclicBehaviour
from spade.message import Message
from spade.template import Template
import sys

# masinstructor@hot-chilli.eu   Instructor001
class InstructorAgent(Agent):
    ready = True
    name = ""

    async def setup(self):
        self.name = "1"
        print("Instructor "+ self.name +" started")
        
        self.add_behaviour(self.PropagateBehav(self))
        self.add_behaviour(self.RequestBehav(self))

    #
    #   Waiting for managers to ask
    #
    class PropagateBehav(CyclicBehaviour):
        def __init__(self, agent):
            super().__init__()
            self.agent = agent

        async def run(self):

            if self.agent.ready == True:
                template = Template()
                template.set_metadata("performative", "propagate")
                msg = await self.receive(timeout=5)
                if msg and msg.get_metadata("performative") == template.get_metadata("performative"):
                    print("Propagate received")
                    rep = Message(to=format(msg.sender))
                    rep.set_metadata("performative", "agree")
                    rep.body = "Ready"
                    print("Sending Agree")
                    await self.send(rep)

    #
    #   process the Request and do the Task
    #
    class RequestBehav(CyclicBehaviour):
        def __init__(self, agent):
            super().__init__()
            self.agent = agent

        async def run(self):
            template = Template()
            template.set_metadata("performative", "request")
            msg = await self.receive(timeout=5)
            if msg and msg.get_metadata("performative") == template.get_metadata("performative"):
                self.agent.ready == False
                print("Request received")
                
                rep = Message(to=format(msg.sender))
                rep.set_metadata("performative", "confirm")
                rep.body = format(self.agent.name)
                await self.send(rep)
                print("Sending Confirm")

                ##########################
                # Section: Instruct Client
                ##########################
                self.agent.ready == True
            

#async def main():
#    Instructor = InstructorAgent("masinstructor@hot-chilli.eu", "Instructor001")
#    await Instructor.start()
#
#    await spade.wait_until_finished(Instructor)
#    print("Agent finished")

#if __name__ == "__main__":
#    spade.run(main())
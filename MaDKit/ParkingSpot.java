package parkingHelp;

import madkit.kernel.AgentAddress;

public class ParkingSpot {
	private boolean free;
	private AgentAddress client;
	
	// Constructor method 
	public ParkingSpot() {									
		this.free = true;
		this.client = null;
	}
	
	// safe client AID on spot
	public void parkClient(AgentAddress client) {								
		this.free = false;
		this.client = client;
	}
	
	// clear spot
	public void clear() {											
		this.free = true;
		this.client = null;
	}
	
	// returns true if free
	public boolean isFree() {											
		return this.free;
	}
	
	// returns client AID
	public AgentAddress getClient() {											
		return this.client;
	}
	
	// returns clients name if spot is taken
	public String getClientName() {										
		if(this.client != null) {
			return this.client.toString();
		}
		return "-";
	}
}

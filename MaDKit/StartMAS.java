package parkingHelp;

import madkit.kernel.Agent;
import madkit.kernel.Madkit;

public class StartMAS extends Agent{
	
	@Override
    protected void activate() {
		new Madkit("--launchAgents","parkingHelp.ManagerAgent,true;", "parkingHelp.ClientAgent,true;", "parkingHelp.InstructorAgent, true;");
	}
	@Override
    protected void live() {
		killAgent(this);
	}
	
	public static void main(String[] args) {
		executeThisAgent(1, true);
	}
}

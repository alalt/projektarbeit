package parkingHelp;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

import madkit.kernel.Agent;
import madkit.kernel.AgentAddress;
import madkit.kernel.Message;
import madkit.message.ACLMessage;

public class ClientAgent extends Agent{
	private int step = 0;
	private int parkingSpotID = -1;
	private AgentAddress parkingManager = null;

	@Override
    protected void activate() {
		pause(5000);
		
		getLogger().setLevel(Level.FINE);
		getLogger().info("Client starting");
        //Take role in society
        createGroup("ParkingHelp", "ParkingHelp");
        requestRole("ParkingHelp", "ParkingHelp", "Client");
	}
	
	@Override
    protected void live() {
		ArrayList<ACLMessage> proposals = new ArrayList<ACLMessage>();
		while (true) {
			switch (step) {
			case 0:
				proposals.clear();
				ACLMessage cfp = new ACLMessage ();
				cfp.setPerformative("REQUEST");
				cfp.setConversationID("ParkingRequest");
				cfp.setContent("Send Proposal");
				broadcastMessage("ParkingHelp", "ParkingHelp", "Manager", cfp);
				getLogger().info("Send REQUESTS");
				
				int counter = 0;
				while (counter < 5 && isMessageBoxEmpty()) {
					pause(2000);
				}
				
				while (isMessageBoxEmpty() == false) {
					Message prop = nextMessage();
					ACLMessage p = null;
					if(prop != null) {
						try {
							p = (ACLMessage) prop;
							
							if (p.getPerformative() == "PROPOSE") {
								getLogger().info("PROPOSE recieved");
								proposals.add(p);
							}
							
						}catch (Exception ex) {
							getLogger().warning("Not an ACL Message in step 0");
						}
					}
				}
				if (proposals.isEmpty() == false) {
					step = 1;
				} else {
					getLogger().warning("No Proposals");
				}
				break;
			case 1:
				// select Proposal
				if(proposals.isEmpty() == false) {
					ACLMessage proposal = proposals.get(0);
					
					ACLMessage msg = new ACLMessage();
					msg.setPerformative("ACCEPT_PROPOSAL");
					msg.setConversationID("Accepting");
					msg.setContent(proposal.getFieldValue("content").toString());
					sendMessage(proposal.getSender(), msg);
					
					parkingSpotID = Integer.valueOf(proposal.getFieldValue("content").toString());
					getLogger().info("Send ACCEPT_PROPOSAL");
					step = 2;
				}
				break;
			case 2:
				while (step == 2)
				{
					Message info = nextMessage();
					ACLMessage p = null;
					if(info != null) {
						try {
							p = (ACLMessage) info;
							
							if (p.getPerformative() == "INFORM") {
								getLogger().info("INFORM recieved");
								getLogger().info("Follow Instructor");
								
								//////////////////////
								// Section: Parking
								//////////////////////
								
								parkingManager = p.getSender();
								step = 3;
								
							} else if (p.getPerformative() == "CANCEL") {
								getLogger().info("Manager cancelt the parking request");
								step = 0;
								parkingSpotID = -1;
							}
							
						}catch (Exception ex) {
							getLogger().warning("Not an ACL Message in step 2");
						}
					}
				}
				break;
			case 3:
				int min = 5000;
				int max = 15000;
				pause(new Random().nextInt((max - min) + 1) + min);
				
				ACLMessage msg = new ACLMessage();
				msg.setPerformative("INFORM");
				msg.setConversationID("Leaving");
				msg.setContent(String.valueOf(parkingSpotID));
				sendMessage(parkingManager, msg);
				getLogger().info("Send INFORM Leaving");
				this.killAgent(this);
				break;
			}
		}
	}
	
	@Override
    protected void end() {
		getLogger().info("Terminating");
		pause(10000);
    }
}

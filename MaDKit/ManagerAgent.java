package parkingHelp;

import java.util.ArrayList;
import java.util.logging.Level;
import madkit.kernel.Agent;
import madkit.kernel.AgentAddress;
import madkit.kernel.Message;
import madkit.message.ACLMessage;


public class ManagerAgent extends Agent{
	
	private ArrayList<ParkingSpot> spots;				// List of parking spots
	private int spotsNumber = 5;						// Number of parking spots
	
	@Override
    protected void activate() {
		//set Logger level
        getLogger().setLevel(Level.FINE);
        getLogger().info("Manager starting");
        //Take role in society
        createGroup("ParkingHelp", "ParkingHelp");
        requestRole("ParkingHelp", "ParkingHelp", "Manager");
        
        //create parking spots
        spots = new ArrayList<ParkingSpot>();
        for (int i = 0;i < spotsNumber;i++) {
        	spots.add(new ParkingSpot());
        }
        getLogger().info("Created " + spotsNumber + " Parking Spots");
    }
	
	@Override
    protected void live() {
		int step = 0;
		AgentAddress clientAddress = null;
		int desiredSpot = -1;
		
		while (true) {
			
			Message msg = nextMessage();
			ACLMessage m = null;
			
			if (msg != null) {
				m = (ACLMessage) msg;
				
				String CID = m.getPerformative().toString();
				switch (CID) {
				
				case "REQUEST":	
					
					getLogger().info("Got REQUEST");
					ACLMessage rep = new ACLMessage();									// Create PROPOSE message for Client
					int freeSpot = getFreeSpot();
					
					if(freeSpot != -1) {												// If a spot in arraylist ist free
						rep.setPerformative("PROPOSE");
						rep.setConversationID(m.getConversationIDentifier());
						rep.setContent(String.valueOf(freeSpot));
						getLogger().info("Send PROPOSE");
					} else {
						rep.setPerformative("CANCEL");
						rep.setConversationID("ParkingRequest");
						rep.setContent(String.valueOf(freeSpot));
						getLogger().warning("No free Spot: Send CANCEL");
					}
					
					sendMessage(msg.getSender(),rep);
					break;
					
				case "ACCEPT_PROPOSAL":	
					
					if (step == 0) {
						getLogger().info("ACCEPT_PROPOSAL recieved");
						ReturnCode code = ReturnCode.NOT_GROUP;
						desiredSpot = Integer.valueOf(m.getFieldValue("content").toString());	
						clientAddress = m.getSender();
						
						if(this.spots.get(desiredSpot).isFree()) {								
							ACLMessage task = new ACLMessage();							//Create REQUEST Message for Instructor
							task.setPerformative("REQUEST");
							task.setConversationID("Task");
							task.setContent(Integer.toString(desiredSpot) + "," + m.getSender().toString());
							int count = 0;
							
							while (count < 5 && code != ReturnCode.SUCCESS) {
								// This will randomly choose a receiver having this role and send request
								code = sendMessage("ParkingHelp", "ParkingHelp", "Instructor", task);
								getLogger().info("Send REQUEST to Instructor");
								count++;
								pause(5000);
						    }
							if(code == ReturnCode.SUCCESS) {
								step = 1;
							} else {
								getLogger().warning("No Instructor found");
							}
						}
					}
					else {
						sendMessage(m.getReceiver(), m);
					}
					break;
					
				case "CONFIRM": // Instructor answer
					
					if(step == 1 && clientAddress != null) {
						getLogger().info("CONFIRM recieved");
						
						ACLMessage info = new ACLMessage();						//Create INFORM message
						info.setPerformative("INFORM");
						info.setConversationID("Instructor");
						info.setContent(m.getSender().toString());
						sendMessage(clientAddress, info);
						getLogger().info("Send INFORM");

						this.spots.get(desiredSpot).parkClient(clientAddress);			//Safe Client to spot
						desiredSpot = -1;
						clientAddress = null;
						step = 0;
						printSpots();
					}
					break;
					
				case "CANCEL": 
					
					if(step == 1 && clientAddress != null) {
						getLogger().info("CANCEL from Instructor recieved");
						
						ACLMessage cancel = new ACLMessage();					//Create CANCEL message for Client if Instructor send cancel
						cancel.setPerformative("CANCEL");
						cancel.setConversationID("Instructor");
						cancel.setContent(m.getSender().toString());
						sendMessage(clientAddress, cancel);
						
						getLogger().info("Send CANCEL");
						desiredSpot = -1;
						clientAddress = null;
						step = 0;
					}
					break;
					
				case "INFORM": 	// leaving
					
					if(m.getConversationIDentifier() == "Leaving") {			//A Client is leaving
						getLogger().info("INFORM Leaving recieved");
						
						spots.get(Integer.valueOf(m.getFieldValue("content").toString())).clear();
						printSpots();
					}
					
					break;
				}
			}
		}//while 
    }
	
	@Override
    protected void end() {
		getLogger().info("Terminating");
    }

	
	/////////////////////////////////////////////////////////////
	// Methods to control the Parkingspot list
	public int getFreeSpot() {
		int ret = -1;
		for(int i = 0 ; i < spotsNumber; i++) {
			if(this.spots.get(i).isFree() == true) {
				ret = i;
				break;
			}				
		}
		return ret;
	}
	
	public void printSpots() {
		for(int i = 0; i < this.spotsNumber; i++) {
			String a = spots.get(i).getClientName();
			getLogger().info("Spot " + i + " : " + a);
		}
		getLogger().info("-----------------------------------");
	}
}


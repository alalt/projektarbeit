package parkingHelp;

import java.util.logging.Level;

import madkit.kernel.Agent;
import madkit.kernel.Message;
import madkit.message.ACLMessage;

public class InstructorAgent extends Agent {
	
	private int spotID;
	
	@Override
    protected void activate() {
		//set Logger level
        getLogger().setLevel(Level.FINE);
        getLogger().info("Instructor starting");
        //Take role in society
        createGroup("ParkingHelp", "ParkingHelp");
        requestRole("ParkingHelp", "ParkingHelp", "Instructor");
	}
	
	@Override
    protected void live() {
		while(true) {
			Message msg = nextMessage();
			ACLMessage m = null;
			String ClientAddress;
			
			if (msg != null) {
				try {
					m = (ACLMessage) msg;
					
					if(m.getPerformative() == "REQUEST") {
						getLogger().info("REQUEST recieved");
						
						String content = m.getFieldValue("content").toString();
						String [] sContent = content.split(",", 2 );
						
						try {
							spotID = Integer.valueOf(sContent[0]);
							ClientAddress = sContent[1];
							
							ACLMessage rep = new ACLMessage();
							rep.setPerformative("CONFIRM");
							rep.setContent(String.valueOf(spotID));
							sendMessage(m.getSender(), rep);
							getLogger().info("Send CONFIRM");
							
							//////////////////////
							// Section: Instruct Client
							//////////////////////
						
						}catch (Exception ex) {
							getLogger().warning("Error: Can not process Content of Message. Send CANCEL");
							ACLMessage rep = new ACLMessage();
							rep.setPerformative("CANCEL");
							rep.setContent("Wrong Information");
							sendMessage(m.getSender(), rep);
						}
					}
					
				} catch (Exception ex) {
					getLogger().warning("No ACL Message");
				}
			}
		}
	}
	
	@Override
    protected void end() {
		getLogger().info("Terminating");
    }
}

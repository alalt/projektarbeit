package test.parkhilfe;

import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.Random;

import jade.core.*;

public class ClientAgent extends Agent{
	private static final long serialVersionUID = 1L;
	
	private AID [] managerAgents;						// Array with all available managers
	private Boolean parking;							// true if this client has a parking spot
	private int parkingSpotID;							// ID of own parking spot
	
	// methods
	protected void setup () {
		parking = false;
		System.out.println("Client " + getAID().getName() + " ready");
		addBehaviour(new GetManagers());
	}
	protected void takeDown () {
		System.out.println("Client " + getAID().getName() + " terminating.");
	}
	
	// ask DF for all available managers
	private class GetManagers extends OneShotBehaviour {
		private static final long serialVersionUID = 1L;
		
		public void action () {
			this.myAgent.doWait(2000);
			if(parking == false) {
				System.out.println("C: DF Search Managers");
				// update the list of seller agents
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType ("Manager");
				template.addServices(sd);
				
				try {
					DFAgentDescription[] result = DFService.search( myAgent , template );
					managerAgents = new AID[ result.length ];
					//safe all Managers in Array
					for ( int i = 0; i < result.length; ++i) {
						managerAgents[i] = result[i].getName();
					} // for
				} catch ( FIPAException fe) {
					fe.printStackTrace();
				} // try, catch
			
				// perform the request
				myAgent.addBehaviour(new RequestPerformer());
			}
		}
	}
	
	// perform Request to all managers
	private class RequestPerformer extends Behaviour {
		private static final long serialVersionUID = 1L;
		
		private ArrayList<AID> ProposingManager = new ArrayList<AID>(); // the Managers who propose
		private ArrayList<String> ProposedParkingSpotIDs = new ArrayList<String>();
		private int repliesCnt = 0; // the counter of replies from Manager agents
		private MessageTemplate mt; // the template to receive replies
		private int step = 1;
		
		public void action() {
			switch(step) {
			
			case 1: 	//Send Request to all Managers
				if (ProposingManager != null) {
					ProposingManager.clear();
					ProposedParkingSpotIDs.clear();
				}
				ACLMessage cfp = new ACLMessage ( ACLMessage.REQUEST );
				for ( int i = 0; i < managerAgents.length; ++i) {
					cfp.addReceiver ( managerAgents[i]);
				} // for
				cfp.setContent( "Car infos" );
				cfp.setConversationId("ParkingRequest");
				cfp.setReplyWith("cfp"+ System.currentTimeMillis()); // Unique value
				System.out.println("C: Sending REQUEST");
				myAgent.send(cfp);
				// prepare the template to get proposals
				mt = MessageTemplate.and( MessageTemplate.MatchConversationId("ParkingRequest"), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
				step = 2;
				break;
					
			case 2:		// receive all proposals/refusals from Manager Agents
				ACLMessage reply = myAgent.receive (mt);
				if ( reply != null ) {
					// reply received
					if ( reply.getPerformative () == ACLMessage.PROPOSE ) {
						//int price = Integer.parseInt ( reply.getContent());
						ProposingManager.add(reply.getSender());
						ProposedParkingSpotIDs.add(reply.getContent());
					} 
					repliesCnt++;
					if ( repliesCnt >= managerAgents.length) {
						// we received all replies
						step = 3;
					} // if [repliesCnt]
				} else {
					block () ;
				} // if, else [reply]			
			
				break;
				
			case 3:		// Accept a Proposal by sending Message to Manager
				///////////////////////////
				// Select Manager Proposal
				///////////////////////////
				ACLMessage accept = new ACLMessage ( ACLMessage.ACCEPT_PROPOSAL );
				accept.addReceiver(ProposingManager.get(0));
				accept.setConversationId("Accept spot");
				accept.setContent(ProposedParkingSpotIDs.get(0));
				System.out.println("C: Sending ACCEPT_PROPOSAL");
				myAgent.send(accept);
				mt = MessageTemplate.MatchContent(ProposedParkingSpotIDs.get(0));
				step = 4;
				break;
				
			case 4:		// Waiting for Information about Instructor
				ACLMessage replyVerwalter = myAgent.receive (mt);
				if ( replyVerwalter != null ) {
					if ( replyVerwalter.getPerformative() == ACLMessage.INFORM ) {
						System.out.println("C: Following Instructor to Spot " + replyVerwalter.getContent());
						//////////////////////
						// Section: Parking
						//////////////////////
						parking = true;
						parkingSpotID = Integer.valueOf(replyVerwalter.getContent());
						step = 5;
					} else if(replyVerwalter.getPerformative () == ACLMessage.CANCEL) {
						step = 1;
						System.out.println("C: Reservation gecancelt");
					}
				} else {
					block ();
				}
				break;
				
			case 5:		//Case 5: Sleep for 10 - 60 seconds and leave
				Random rn = new Random();
				try {
					Thread.sleep(rn.nextInt((15000 - 5000) + 1) + 5000);
				} catch (Exception ex) {
				
				}
				if(parking == true) {
					ACLMessage leave = new ACLMessage ( ACLMessage.INFORM );
					leave.addReceiver(ProposingManager.get(0));
					leave.setConversationId("Leaving");
					leave.setContent(String.valueOf(parkingSpotID));
					System.out.println("C: Sending Leaving info");
					myAgent.send(leave);
					//////////////////////////////////
					// Section: Leaving
					/////////////////////////////////
					step = 6;
					this.myAgent.doDelete();
				}
				break;
			}
		}
		
		public boolean done () {
			if ( step == 3 && ProposingManager.isEmpty() ) {
				System.out.println("C: No Manager proposals") ;
			} 
			return step == 6;
		} // done()

	}
}

package test.parkhilfe;

import jade.core.AID;
	
public class ParkingSpot {
	private boolean free;
	private AID client;
	
	// Constructor method 
	public ParkingSpot() {									
		this.free = true;
		this.client = null;
	}
	
	// safe client AID on spot
	public void parkClient(AID client) {								
		this.free = false;
		this.client = client;
	}
	
	// clear spot
	public void clear() {											
		this.free = true;
		this.client = null;
	}
	
	// returns true if free
	public boolean isFree() {											
		return this.free;
	}
	
	// returns client AID
	public AID getClient() {											
		return this.client;
	}
	
	// returns clients name if spot is taken
	public String getClientName() {										
		if(this.client != null) {
			return this.client.getName();
		}
		return "-";
	}
}

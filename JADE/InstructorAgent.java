package test.parkhilfe;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class InstructorAgent extends Agent{
	private static final long serialVersionUID = 1L;
	
	private boolean registered;
	private int step;
	private String clientAIDName;
	private int spotID;
	
	protected void setup() {
		System.out.println("Instructor " + getAID().getName() + " starting");
		
		addBehaviour(new RequestServer());
		register();
		
	}
	
	protected void takeDown() {
		if(registered == true) {
			deregister();
		}
		System.out.println("Instructor " + getAID().getName() + " terminating.");
	}

	private void register () {
		DFAgentDescription dfd = new DFAgentDescription () ;
		dfd.setName ( getAID () ) ;
		
		ServiceDescription sd = new ServiceDescription () ;
		sd.setType ("Instructor") ;
		sd.setName ("Parking Instructor") ;
		dfd.addServices (sd) ;
		clientAIDName = null;
		spotID = -1;
		
		try {
			DFService.register (this , dfd ) ;
			registered = true;
		} catch ( FIPAException fe) {
			fe. printStackTrace () ;
		}
	}
	
	private void deregister () {
		try {
			DFService.deregister ( this ) ;
			registered = false;
		} catch ( FIPAException fe) {
			fe. printStackTrace () ;
		} // try, catch
	}

	// process the request and start behaviour instructClient
	private class RequestServer extends CyclicBehaviour {
		private static final long serialVersionUID = 1L;
		public void action () {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative( ACLMessage.REQUEST ), MessageTemplate.MatchConversationId("Task"));
			ACLMessage msg = myAgent.receive(mt);
			if (msg != null) {
				String content = msg.getContent();
				String [] sContent = content.split(",", 2);
				clientAIDName = sContent[0];
				spotID = Integer.valueOf(sContent[1]);
				ACLMessage reply = msg.createReply();
				
				reply.setPerformative(ACLMessage.CONFIRM);
				reply.setContent(String.valueOf(spotID));
				
				myAgent.send(reply);
				System.out.println("I: Sending CONFIRM ");
				step = 1;
				addBehaviour(new InstructClient());
			} else {
				block();
			}
		}
	}
	
	// process the request by instructing the client
	private class InstructClient extends Behaviour {
		private static final long serialVersionUID = 1L;
		public void action () {
			switch (step) {
			case 1:
				if (registered == true)
					deregister();												// deregister this Instructor at DF because he is busy
				System.out.println("I: instructing Client " + clientAIDName);
				///////////////////////////
				// Section: Move to Client
				///////////////////////////
				step++;
				break;
			case 2:
				///////////////////////////
				// Section: lead the client to the parking spot
				///////////////////////////
				step++;
				break;
			case 3:
				///////////////////////////
				// Section: Move back to waiting location
				///////////////////////////
				if (registered == false)
					register();													// register at DF
				step++;
				break;
			}
		}
		
		public boolean done () {
			if (step == 4) {
				return true;
			}
			return false;
		}
	}
}

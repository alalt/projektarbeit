package test.parkhilfe;

import jade.core.*;
import jade.domain.*;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPAAgentManagement.*;
import jade.lang.acl.*;
import java.util.ArrayList;


public class ManagerAgent extends Agent{
	private static final long serialVersionUID = 1L;
	
	private AID [] instructorAgents;				// Array with all available Instructors
	private AID clientAID;							// AID of current client
	private ArrayList<ParkingSpot> spots;			// List of parking spots
	private int spotNumber = 5;						// Number of parking spots
	
	///////////////////////////////////////////////////////
	// All Agent start and end methods
	protected void setup() {
		// printout a welcome message
		System.out.println ("Manager "+ getAID().getName() +" is ready.") ;
		
		this.spots = new ArrayList<ParkingSpot>();
		for (int i = 0; i < spotNumber; i++) {
			this.spots.add(new ParkingSpot());
		}
		
		// register the parking service in the yellow pages
		register () ;
		// Answer all REQUEST messages
		addBehaviour (new RequestServer () ) ;
		// Answer all ACCEPT_PROPOSAL messages and request an instructor
		addBehaviour (new ManagerServer () ) ;
		// Inform the requesting car about instructor
		addBehaviour (new InfoServer () ) ;
		// Receiving Leaving Messages from Client
		addBehaviour (new LeavingServer () ) ;
	}
	
	protected void takeDown () {
		// de-register from the yellow pages
		deregister () ;
		// printout a dismissal message
		System.out.println ("Manager "+ getAID().getName() +" terminating.") ;
	}
	
	private void register () {
		DFAgentDescription dfd = new DFAgentDescription () ;
		dfd.setName ( getAID () ) ;
		ServiceDescription sd = new ServiceDescription () ;
		sd.setType ("Manager") ;
		sd.setName ("Parking Manager") ;
		dfd.addServices (sd) ;
		
		try {
			DFService.register (this , dfd ) ;
		} catch ( FIPAException fe) {
			fe. printStackTrace () ;
		}
	}
	
	private void deregister () {
		try {
			DFService.deregister ( this ) ;
		} catch ( FIPAException fe) {
			fe. printStackTrace () ;
		} // try, catch
	}	
	
	///////////////////////////////////////////////////////
	// All Manager behaviours
	private class RequestServer extends CyclicBehaviour {
		private static final long serialVersionUID = 1L;
		private MessageTemplate mt;
		
		public void action () {
			mt = MessageTemplate.and(MessageTemplate.MatchPerformative( ACLMessage.REQUEST ), MessageTemplate.MatchConversationId("ParkingRequest")) ;
			
			ACLMessage msg = myAgent.receive (mt);
			if (msg != null ) {
				ACLMessage reply = msg.createReply();
				//Check for free parking spot
				int freeSpot = checkFreeSpot();
				if(freeSpot != -1) {
					reply.setPerformative(ACLMessage.PROPOSE);
					reply.setConversationId("ParkingRequest");
					reply.setContent(String.valueOf(freeSpot));
					System.out.println("M: Sending PROPOSE");
				} else {
					reply.setPerformative(ACLMessage.CANCEL);
					reply.setContent("No free Spots");
					System.out.println("M: Sending Cancel");
				}
				myAgent.send( reply );
			} else {
				block ();
			} // if, else [msg]
		} // action()
	}
	
	private class ManagerServer extends CyclicBehaviour {
		private static final long serialVersionUID = 1L;
		private int desiredSpotID;
		private MessageTemplate mt;
		private int step = 1;							// step for ManagerServer behaviour
		
		public void action () {
			switch (step) {
			case 1:
				
				mt = MessageTemplate.and( MessageTemplate.MatchConversationId("Accept spot"), MessageTemplate.MatchPerformative( ACLMessage.ACCEPT_PROPOSAL ));
				ACLMessage msg = myAgent.receive (mt) ;
				if (msg != null) {
					try {
						desiredSpotID = Integer.valueOf(msg.getContent());
					} catch(NumberFormatException ex) {
						System.out.println("M: Got incorrect SpotID; @ManagerServer Step 1");
						block();
					}
					
					// Check if the desired Spot is free
					if(spots.get(desiredSpotID).isFree()) {
						clientAID = msg.getSender();
						step = 2;
					} else {
						block();
					}
					
				} else {
					block();
				}
				break;
			case 2:
				//Ask all registered Instructors from Directory Faciliator 
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType ("Instructor");
				template.addServices(sd);
				
				try {
					System.out.println("M: DF search Instructors");
					DFAgentDescription[] result = DFService.search( myAgent , template );
					instructorAgents = new AID[ result.length ];
					for ( int i = 0; i < result.length; ++i) {
						instructorAgents[i] = result[i].getName();
					} // for
				} catch ( FIPAException fe) {
					fe.printStackTrace();
				} // try, catch
				//
				if (instructorAgents != null && instructorAgents.length > 0) {
					step = 3;
				} else {
					ACLMessage info = new ACLMessage ( ACLMessage.CANCEL );
					info.addReceiver ( clientAID);
					info.setConversationId("Info");
					info.setContent(" ");
					myAgent.send(info);
					System.out.println("M: No Instructor available. Sending CANCEL");
					step = 1;
				}
				break;
			case 3:
				ACLMessage task = new ACLMessage ( ACLMessage.REQUEST );
				task.addReceiver ( instructorAgents[0]);
				task.setConversationId("Task");
				task.setContent( clientAID.getName() + "," + desiredSpotID); // AID of RequestingCar
				task.setReplyWith("task"+ System.currentTimeMillis() + clientAID.getName()); // Unique value
				System.out.println("M: Sending REQUEST to Instructor");
				myAgent.send(task);
				step = 1;
				break;		
			}
		}
	}

	private class InfoServer extends CyclicBehaviour {
		private static final long serialVersionUID = 1L;
		private MessageTemplate mt;
		
		public void action () {
			mt = MessageTemplate.MatchConversationId("Task");
			ACLMessage reply = myAgent.receive (mt);
			if (reply != null) {
				if (reply.getPerformative () == ACLMessage.CONFIRM) {
					int SpotID = -1;
					try {
						SpotID = Integer.valueOf(reply.getContent());
						ACLMessage info = new ACLMessage ( ACLMessage.INFORM );
						info.addReceiver ( clientAID);
						info.setConversationId(reply.getSender().getName());
						info.setContent(Integer.toString(SpotID)); // AID of Instructor
						myAgent.send(info);
						System.out.println("M: Sending INFORM to Client");
					} catch(NumberFormatException ex) {
						System.out.println("M: Got incorrect SpotID; @InfoServer");
						block();
					}
					
					
					//Safe Client to Spot
					if(SpotID != -1) {
						System.out.println("M: Safe Client to Spot");
						spots.get(SpotID).parkClient(clientAID);
						printSpots();
					}
				}
			} else {
				block () ;
			} // if, else [reply]
			
		}		
	}
	
	private class LeavingServer extends CyclicBehaviour {
		private static final long serialVersionUID = 1L;
		private MessageTemplate mt;
		
		public void action () {
			
			mt = MessageTemplate.MatchConversationId("Leaving");
			ACLMessage msg = myAgent.receive (mt);
			int parkingSpot = -1;
			if (msg != null && msg.getPerformative () == ACLMessage.INFORM) {
				
				try  {
					parkingSpot = Integer.valueOf(msg.getContent());
				} catch (NumberFormatException ex) {
					System.out.println("M: Got incorrect SpotID; @LeavingServer");
				}
				
				if(parkingSpot != -1) {
					spots.get(parkingSpot).clear();
					printSpots();
				} else {
					for(int i = 0; i < spots.size(); i++) {
						if(spots.get(i).getClient() == msg.getSender()) {
							spots.get(i).clear();
							printSpots();
							break;
						}
					}
				}
			} else {
				block () ;
			} // if, else [reply]
		}
	}
	
	/////////////////////////////////////////////////////////////
	// Methods to control the Parkingspot list
	public int checkFreeSpot() {
		int ret = -1;
		for(int i = 0 ; i < spots.size(); i++) {
			if(this.spots.get(i).isFree() == true) {
				ret = i;
				break;
			}				
		}
		return ret;
	}
		
	public void printSpots() {
		for(int i = 0; i < spots.size(); i++) {
			String a = spots.get(i).getClientName();
			System.out.println("Spot " + i + " : " + a);
		}
		System.out.println("-------------------------------");
	}
}
